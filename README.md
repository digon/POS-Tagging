# Part_Of_Speech_Tagging

This is the project which we undertook as a part of the semester assignment for the course CSN - 371 (Artificial Intelligence). 

Team Members :   
Aditya Kumar Singh [18116003]   
Anmol Agarwal [18116013]   
Saksham Sharma [18116067]   

Instructor :   
[Raksha Sharma](https://www.iitr.ac.in/~CSE/Raksha_Sharma)

You can download the [Test and Train files](https://drive.google.com/drive/folders/1FUxfH2ITpn4K7Gm-XxTHjL0ptMF6uQEn?usp=sharing)   
